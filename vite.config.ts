import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { createHtmlPlugin } from 'vite-plugin-html'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    createHtmlPlugin({
      inject: {
        data: {
          amapScript: '<script src="https://webapi.amap.com/maps?v=1.4.15&key=c26a2cf34fd56b730f200b5e63eaaec1"></script>'
        }
      }
    })
  ],
  // 在define中添加AMap全局变量
  define: {
    'window.AMap': 'AMap'
  },
  server: {
    proxy: {
      // 代理配置
      '/api': {
        target: 'http://localhost:9000',  // 后端服务地址
        changeOrigin: true, // 必须设置为 true，以便代理服务器发送请求头中的 host 与 target 匹配
        rewrite: path => path.replace(/^\/api/, '') // 将请求地址中的 /api 替换为空，适用于后端不需要 /api 前缀的情况
      }
    }
  }
})
