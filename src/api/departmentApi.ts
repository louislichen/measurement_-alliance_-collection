import request from '../until/request.ts';
import { el } from 'element-plus/es/locale';

export interface Filters {
    name: string;
    status: number;//部门状态 0：正常 1：停用
    
  }
 

  export interface DepartmentItem {
    name: string;
    status: number;
    index: number;
    company_name: string;
    create_time: string;
    id: number;
    
  }
  export interface Company{
    companyId: number;
    name: string;
    departmentList: DepartmentItem[];
  }
  interface BackResponse<T> {
    code: number;
    data: T;
    message: string;
  }
  export interface ContactInfo {
    name: string;
    phone:string;
    email:string;
  };
  export const getCompanyList = async (): Promise<Company[]> => {
    const response = await request.get<BackResponse<Company[]>>('/department/list');
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '获取部门列表失败');
    }
  };
  
  export const addDepartmentService = async (department: DepartmentItem,contact:ContactInfo): Promise<string> => {
    console.log(department);
    //添加测盟汇的租户，相当于注册公司，同时添加联系人信息
    if(department.company_name==='测盟汇'){
      const payload = { 
        company_name: department.name,
        user_name:contact.name,
        phone:contact.phone,
        email:contact.email,
        status:department.status
        
      };
      const response = await request.post<BackResponse<string>>('/company/addInDepartment', payload);
      if (response.data.code === 0) {
        return response.data.message; // 返回消息
      } else {
        throw new Error(response.data.message || '获取部门列表失败');
      }
    }else{
      //添加某个公司的部门 联系人信息不是必须的，暂时好像没有用到
      const response = await request.post<BackResponse<string>>('/department/add', department);
      if (response.data.code === 0) {
        return response.data.message; // 返回消息
      } else {
        throw new Error(response.data.message || '获取部门列表失败');
      }
    }
    
  }
  export const updateDepartmentService = async (department: DepartmentItem,contact:ContactInfo): Promise<void> => {
   //如果department.company_name是测盟汇，说明是修改公司名字
    if(department.company_name==='测盟汇'){ 
      const payload = { 
        id: department.id ,
        name:department.name,
        user_name:contact.name,
        phone:contact.phone,
        email:contact.email
      };

      const response = await request.post<BackResponse<void>>('/company/updateById', payload);
      if (response.data.code !== 0) {
         throw new Error(response.data.message || '修改租户失败');
      }
    }else{
      const response = await request.post<BackResponse<void>>('/department/update', department);
    
      if (response.data.code !== 0) {
        throw new Error(response.data.message || '编辑部门信息失败');
      }
    }
   
  }
  //删除部门
  export const deleteDepartmentService = async(name: string,company_name:string): Promise<void>=> {
    const payload = { name: name ,company_name:company_name};

    const response = await request.post<BackResponse<void>>('/department/delete', payload);
    if (response.data.code !== 0) {
      throw new Error(response.data.message || '删除部门失败');
    }
  }

  
  //获取联系人信息，用于编辑界面
  export const getContactInfo = async (department: DepartmentItem): Promise<ContactInfo> => {
    //如果department.company_name是测盟汇，说明此时里面信息是租户信息，则需要获取联系人信息
    if(department.company_name==='测盟汇'){
      const payload = { id: department.id };
      const response = await request.post<BackResponse<ContactInfo>>('/company/listUserByCompanyId', payload);
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '获取联系人信息失败');
    }
    }else{
      return {name:'',phone:'',email:''};
    }
    
  };

  //搜索部门
  export const searchDepartmentService = async (filters: Filters): Promise<Company[]> => {
    const payload = { 
      name: filters.name,
      status:filters.status
     };
    const response = await request.post<BackResponse<Company[]>>('/department/search', payload);
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '搜索部门失败');
    }
  };

  