import request from '../until/request.ts';

export interface Filters {
  company_name: string;
  contacts: string;
  phone: string;
  companyId: number;
}
export interface CompanyItem {
  companyId: number;
  company_name: string;
  user_name: string;
  imageURL: string;
  phone: string;
  contacts: string;
  remark: string;

}
export interface CpyItem {
  id: number;
  company_name: string;
}
export interface UserItem {

  name: string;
  pwd: string;
  role: string;
  status: boolean;
  createTime: string;
  tel: string;
  remark: string;
  imageURL: string;
  companyName: string;
  contacts: string;
  nickname: string;
  post: string;
  email: string;
  gender: string;
  employeeRole: number;
  companyId: number;
  userId: number;
  id: number;

}
interface BackResponse<T> {
  code: number;
  data: T;
  message: string;
  isOk: boolean;
  msg: string;
}
//获取用户
export const getCompanyList = async (): Promise<UserItem[]> => {
  const response = await request.get<BackResponse<UserItem[]>>('/company/list');
  if (response.data.code === 0) {
    return response.data.data; // 返回公司管理员
  } else {
    throw new Error(response.data.message || '获取公司管理员列表失败');
  }
};
//获取公司
export const getListCompany = async (): Promise<CpyItem[]> => {
  const response = await request.get<BackResponse<CpyItem[]>>('/company/listCompany');
  console.log(response.data);
  if (response.data.code === 0) {
    return response.data.data; // 返回公司名称
  } else {
    throw new Error(response.data.message || '获取公司列表失败');
  }
};
export const addCompanyService = async (company: CompanyItem): Promise<BackResponse<string>> => {
  const response = await request.post<BackResponse<string>>('/company/add', company);
  return response.data; // 返回消息
}
export const editCompanyService = async (company: CompanyItem): Promise<BackResponse<string>> => {
  const response = await request.post<BackResponse<string>>('/company/update', company);
  return response.data; // 返回消息
}
export const deleteCompanyService = async (id: number): Promise<BackResponse<string>> => {
  const payload = { id: id };
  const response = await request.post<BackResponse<string>>('/company/delete', payload);
  return response.data; // 返回消息
}
export const exportFile = async (): Promise<void> => {
  try {
    const response = await request({
      url: '/export/company',
      method: 'GET',
      responseType: 'blob',  // 重要：确保响应类型为 'blob'
    });

    // 创建一个下载链接并模拟点击，从而实现文件下载
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'Rentor.xlsx');  // 为下载的文件指定默认文件名
    document.body.appendChild(link);
    link.click();

    // 清理创建的URL和链接元素
    link.parentNode.removeChild(link);
    window.URL.revokeObjectURL(url);
  } catch (error) {
    console.error('导出失败:', error);
    throw new Error('导出失败');
  }
}
export const searchCompanyService = async (company: Filters): Promise<UserItem[]> => {
  const response = await request.post<BackResponse<UserItem[]>>('/company/search', company);
  if (response.data.code === 0) {
    return response.data.data; // 返回列表数据
  } else {
    throw new Error(response.data.message || '获取新闻列表失败');
  }
}