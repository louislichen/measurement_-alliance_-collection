import request from '../until/request.ts';

export interface Filters {
    name: string;
    tel: string;
    status: boolean;
    createTime: string;
  }
export interface UserItem {
   
    name: string;
    pwd: string;
    role: string;
    status: boolean;
    createTime: string;
    tel: string;
    remark: string;
    imageURL: string;
    companyName: string;
    contacts: string;
    nickname: string;
    post: string;
    email: string;
    gender: boolean;
    employeeRole: number;
    companyId: number;
    departmentId: number;
   
    id: number;

}
export interface DepartmentItem {
  name: string;
  status: number;
  index: number;
  company_name: string;
  create_time: string;
  id: number;
}
export interface Company{
  companyId: number;
  name: string;
  departmentList: DepartmentItem[];
}
export interface Stats{
  curOnline: number;
  total: number;
}
interface BackResponse<T> {
    code: number;
    data: T;
    message: string;
    isOk: boolean;
    msg: string;
  }

  export const getCompanyList = async (): Promise<Company[]> => {
    const response = await request.get<BackResponse<Company[]>>('/department/list');
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '获取部门列表失败');
    }
  };
  export const getStats = async (): Promise<Stats[]> => {
    const response = await request.get<BackResponse<Stats[]>>('/user/stats');
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '获取用户统计数据失败');
    }
  };
  export const getUserList = async (): Promise<UserItem[]> => {
    const response = await request.get<BackResponse<UserItem[]>>('/user/list');
    if (response.data.isOk=== true) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.msg || '获取部门列表失败');
    }
  };
  export const getUserListByClickTree = async (companyId:number,id:number): Promise<UserItem[]> => {
    const payload = { id: id,companyId:companyId}; 
    const response = await request.post<BackResponse<UserItem[]>>('/user/searchById', payload);
    if (response.data.code == 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.msg || '获取部门列表失败');
    }
  };
  export const getDepartmentList = async (name:string): Promise<DepartmentItem[]> => {
    const payload = { name: name }; 
    const response = await request.post<BackResponse<DepartmentItem[]>>('/department/findByName', payload);
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '获取部门列表失败');
    }
  };
  export const editUserService = async (user: UserItem): Promise<void> => {
    const response = await request.post<BackResponse<void>>('/user/update', user);
    
    if (response.data.code !== 0) {
      throw new Error(response.data.message || '编辑新闻失败');
    }
  }
  export const addUserService = async (user: UserItem): Promise<BackResponse<string>> => {
    const response = await request.post<BackResponse<string>>('/user/addInfo', user);
    return response.data; // 返回消息
  }
  export const editUserService1 = async (user: UserItem): Promise<BackResponse<string>> => {
    const response = await request.post<BackResponse<string>>('/user/update1', user);
    return response.data; // 返回消息
  }
  export const deleteUserService = async(user: UserItem): Promise<BackResponse<string>>=> {
    
    const response = await request.post<BackResponse<string>>('/user/deleteById', user);
    return response.data; // 返回消息
  }
  export const exportFile = async (): Promise<void> => {
    try {
      const response = await request({
        url: '/export/user',
        method: 'GET',
        responseType: 'blob',  // 重要：确保响应类型为 'blob'
      });
  
      // 创建一个下载链接并模拟点击，从而实现文件下载
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'User.xlsx');  // 为下载的文件指定默认文件名
      document.body.appendChild(link);
      link.click();
  
      // 清理创建的URL和链接元素
      link.parentNode.removeChild(link);
      window.URL.revokeObjectURL(url);
    } catch (error) {
      console.error('导出失败:', error);
      throw new Error('导出失败');
    }
    }
    export const searchUserService = async (user: Filters): Promise<UserItem[]> => {
      const response = await request.post<BackResponse<UserItem[]>>('/user/search', user);
      if (response.data.code === 0) {
        return response.data.data; // 返回列表数据
      } else {
        throw new Error(response.data.message || '获取新闻列表失败');
      }
    }
    //搜索部门
  export const searchDepartmentService = async (name: string): Promise<Company[]> => {
    const payload = { 
      name: name,
      status:null
     };
    const response = await request.post<BackResponse<Company[]>>('/department/search', payload);
    if (response.data.code === 0) {
      return response.data.data; // 返回列表数据
    } else {
      throw new Error(response.data.message || '搜索部门失败');
    }
  };