import request from '../until/request.ts';
interface BackResponse<T> {
    code: number;
    data: T;
    message: string;
  }
export const uploadPicture = async (formData: FormData): Promise<string> => {  
    const response = await request.post<BackResponse<string>>('/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  
    if (response && response.data && response.data.code === 0) {
      return response.data.data;
    } else {
      throw new Error(response.data.message || '上传图片失败');
    }
  };