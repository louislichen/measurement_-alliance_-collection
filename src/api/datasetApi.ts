import request from '../until/request.ts';

interface DatasetItem {
    id: number;
    action: string;
    count: number;
}

interface BackResponse<T> {
    code: number;
    data: T;
    message: string;
}

export const getDataset = async (): Promise<DatasetItem[]> => {
    const response = await request.post<BackResponse<DatasetItem[]>>('/dataset/getData', {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });

    if (response) {
        return response.data.data;
    } else {
        throw new Error(response.data.msg);
    }
};
