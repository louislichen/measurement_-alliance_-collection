import { createRouter, createWebHistory } from 'vue-router'
import Login from '../pages/login.vue'
import SignUp from '../pages/signup.vue'
import Home from '../pages/homeView.vue'

const router = createRouter ({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: Login
        },
        {
            path: '/signup',
            component: SignUp
        },
        {
            path: '/home',
            component: Home,
            children: [
                {
                    path: '',
                    component: () => import('../components/home/home_body.vue')
                },
                {
                    path: 'user',
                    component: () => import('../components/users/user.vue')
                },
                {
                    path: 'meeting',
                    component: () => import('../components/meeting/meetingView.vue')
                },
                {
                    path: 'news',
                    component: () => import('../components/news/newsView.vue')
                },
                {
                    path: 'department',
                    component: () => import('../components/departments/departView.vue')
                },
                {
                    path: 'course',
                    component: () => import('../components/course/courseView.vue')
                },
                {
                    path: 'rentor',
                    component: () => import('../components/rentor/rentorView.vue')
                }
            ]
        }
    ]
})

export default router;