//定制请求的实例

//导入axios  npm install axios
import axios from 'axios';
import { ElMessage } from 'element-plus'

import {useTokenStore} from '../store/token.ts'
import  router from '../router/index.ts'

const baseURL = '/api';
const request = axios.create({baseURL})
//添加响应拦截器
request.interceptors.response.use(
    result => {
        if (result.data.code === 0 || result.data.isOk === true) {
            return result;
        }
        
        // 处理业务错误
        if(result.data.msg&& result.data.isOk===false){
            ElMessage.error(result.data.msg);
            return Promise.reject(new Error(result.data.msg || '服务异常'));
        }else if(result.data.message&& result.data.code===1){
        ElMessage.error(result.data.message ? result.data.message: '服务异常');
        return Promise.reject(new Error(result.data.message || '服务异常'));
        }else{
            return result;
        }

    },
    error => {
        if (error.response) {
            switch (error.response.status) {
                case 401:
                    // 未认证处理
                    ElMessage.error('请先登录');
                    setTimeout(() => {
                        router.push('/');
                    }, 1000);
                    break;
                case 403:
                    // 无权限处理
                    ElMessage.error('没有权限执行此操作');
                    break;
                case 404:
                    // 请求资源不存在
                    ElMessage.error('请求的资源不存在');
                    break;
                default:
                    // 其他错误
                    ElMessage.error('服务异常');
                    break;
            }
        } else if (error.request) {
            // 请求被发出，但没有收到响应
            ElMessage.error('网络请求未响应');
        } else {
            // 发生了其他类型的错误
            ElMessage.error('请求错误: ' + error.message);
        }
        
        return Promise.reject(error); // 这里返回错误，让单独的请求能够捕获并处理
    }
);

//添加请求拦截器
request.interceptors.request.use(
    (config)=>{
        //config是请求的配置对象
        //如果有token,则添加到请求头中
        const tokenStore=useTokenStore()
        if(tokenStore.token){
            config.headers['Authorization']=tokenStore.token
        }
        return config;
    },
    (err)=>{
        return Promise.reject(err);
    }
)

export default request;