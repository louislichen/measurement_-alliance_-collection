import Compressor from 'compressorjs';


export const compressImage = (file: File): Promise<File> => {
    return new Promise((resolve, reject) => {
        new Compressor(file, {
            quality: 0.6,
            maxWidth: 400,
            maxHeight: 400,
            convertSize: 5000000,
            success(result) {
                 // 确保创建 File 对象时指定正确的类型
                 const compressedFile = new File([result], file.name, { type: result.type || file.type });
                 resolve(compressedFile);
            },
            error(err) {
                reject(err);
            }
        });
    });
};


