import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useTokenStore = defineStore('token', () => {
  const token = ref<string>('');  // 明确 token 是字符串类型

  const setToken = (newToken: string) => {
    token.value = newToken;
  };

  const removeToken = () => {
    token.value = '';
  };

  return {
    token,
    setToken,
    removeToken
  };
}, {
  persist: {
    enabled: true,  // 启用持久化
    strategies: [{  // 定义持久化策略
      key: 'myTokenStore',  // 在 localStorage 中使用的键名
      storage: window.localStorage  // 定义使用的存储对象
    }]
  }
});
