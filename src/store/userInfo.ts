// 定义用户信息的接口
 export interface UserInfo {
  name: string;
  role:string;
  company_name: string;
  companyId: number;
  departmentId: number;
  id: number;
}

import { defineStore } from "pinia";
import { ref } from "vue";

// 定义使用 Pinia store 的类型安全方式
export const useUserInfoStore = defineStore(
  "userInfo",
  () => {
    // 使用接口定义的类型来初始化 ref
    const info = ref<UserInfo>({
      name: "",
      role: "",
      company_name: "",
      companyId: null,
      departmentId: null,
      id: null
    });

    // 添加类型注解来增强参数的类型安全
    const setInfo = (newInfo: UserInfo) => {
      info.value = newInfo;
    };

    // 清空用户信息
    const removeInfo = () => {
      info.value = {
        name: "",
        role: "",
        company_name: "",
        companyId: null,
        departmentId: null,
        id: null
      };
    };

    return { info, setInfo, removeInfo };
  },
  {
    persist: {
        enabled: true,
        strategies: [
          {
            key: 'userInfo',
            storage: localStorage // 或者使用 sessionStorage
          }
        ]
      }
  }
);
