import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import locale from  'element-plus/dist/locale/zh-cn.js'
import './until/request.ts';
import piniaPluginPersist from 'pinia-plugin-persist';

// 创建应用
const app = createApp(App)
// 创建pinia
const pinia = createPinia()
pinia.use(piniaPluginPersist);
// 安装插件
app.use(pinia).use(router).use(ElementPlus,{locale})
// 安装路由器
app.use(router)
// app.config.globalProperties.$echarts = echarts
// 挂载应用
app.mount('#app')
